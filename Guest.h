//
// Created by chitchat on 3/27/20.
//

#ifndef LAB4__GUEST_H_
#define LAB4__GUEST_H_

#include "Human.h"
class Guest : public Human {
 public:
  Guest(std::string name, unsigned short age, char gender, unsigned short roomNum);
  void settle();
  void unsettle();
  unsigned short RoomNum();
  void RoomNum(unsigned short roomNum);
  void seeText();
 protected:
  unsigned short roomNum{};
};

#endif //LAB4__GUEST_H_
