//
// Created by chitchat on 3/27/20.
//

#ifndef LAB4__ROOM_H_
#define LAB4__ROOM_H_

#include <vector>
#include "Guest.h"

class Room {
 public:
  Room(unsigned int num, unsigned short capacity);
  ~Room();
  void AddGuest(Guest guest);
  void ShowGuests();
  unsigned short Capacity();
  void Capacity(unsigned short capacity);

 protected:
  unsigned short capacity;
  bool busy{false};
  std::vector<Guest> guests;

};

#endif //LAB4__ROOM_H_
