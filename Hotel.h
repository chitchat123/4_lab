//
// Created by chitchat on 3/27/20.
//

#ifndef LAB4__HOTEL_H_
#define LAB4__HOTEL_H_

#include <vector>
#include "Employee.h"
#include "Room.h"
class Hotel {
 public:
  Hotel(std::string address);
  void AddEmployee();
  void ShowEmployees();
  void AddRoom();
  void ShowRooms();
  ~Hotel();

 protected:
  std::vector<Room> rooms;
  std::vector<Employee> employees;
  std::string address;

};

#endif //LAB4__HOTEL_H_
