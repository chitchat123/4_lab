//
// Created by chitchat on 3/27/20.
//

#include <iostream>
#include "Room.h"

Room::~Room() = default;
Room::Room(unsigned int num, unsigned short capacity) : capacity(capacity) {

}
void Room::AddGuest(Guest guest) {
  std::cout << this->guests.size();
  if (this->guests.size() > this->capacity) {
    this->guests.push_back(guest);
  }
}
void Room::ShowGuests() {
  std::cout << "List of guests";
  for (int i = 0; i < this->guests.size(); ++i) {
    std::cout << "\n\nName: " << this->guests[i].Name();
    std::cout << "\nAge: " << this->guests[i].Age();
    std::cout << "\nGender: " << this->guests[i].Gender();
    std::cout << "\nRoom number: " << this->guests[i].RoomNum();
  }
}
unsigned short Room::Capacity() {
  return this->capacity;
}
void Room::Capacity(unsigned short capacity) {
  this->capacity = capacity;
}
