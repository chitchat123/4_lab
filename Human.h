//
// Created by chitchat on 3/27/20.
//

#ifndef LAB4__HUMAN_H_
#define LAB4__HUMAN_H_

#include <string>
#include <vector>
#include <set>
#include <cmath>
#include <map>
#include <string>
class Human {
 public:
  Human(std::string name, unsigned short age, char gender);
  std::string Name();
  void Name(std::string name);
  unsigned short Age();
  void Age(unsigned short age);
  char Gender();
  void Gender(char gender);
 private:
  std::string name;
  unsigned short age;
  char gender;


};

#endif //LAB4__HUMAN_H_
