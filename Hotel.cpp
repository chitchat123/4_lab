//
// Created by chitchat on 3/27/20.
//

#include <iostream>
#include <algorithm>
#include "Hotel.h"

Hotel::Hotel(std::string address) : address(address) {

}
void Hotel::AddEmployee() {
  std::string name;
  unsigned short age;
  char gender;
  std::string position;
  std::cout << "Enter employee name (string): ";
  std::cin >> name;
  std::cout << "Enter employee age(unsigned char): ";
  std::cin >> age;
  std::cout << "Enter employee gender(single char f or m): ";
  std::cin >> gender;
  std::cout << "Enter employee position(string): ";
  std::cin >> position;
  this->employees.emplace_back(name, age, gender, position);
}
void Hotel::ShowEmployees() {
  for (auto & employee : this->employees) {
    std::cout << "\n\nName: " << employee.Name();
    std::cout << "\nAge: " << employee.Age();
    std::cout << "\nGender: " << employee.Gender();
    std::cout << "\nPosition: " << employee.Position();
  }
}
void Hotel::AddRoom() {
  bool adding;
  unsigned int roomNum;
  unsigned short capacity;
  std::cout << "Enter room number: ";
  std::cin >> roomNum;
  std::cout << "Enter room capacity: ";
  std::cin >> capacity;
  this->rooms.emplace_back(Room(roomNum, capacity));
  for (int i = 0; i < capacity; ++i) {
    std::string name;
    unsigned short age;
    char gender;
    std::string position;
    std::cout << "Enter guest name (string): ";
    std::cin >> name;
    std::cout << "Enter guest age(unsigned char): ";
    std::cin >> age;
    std::cout << "Enter guest gender(single char f or m): ";
    std::cin >> gender;
    std::cout << "Start";
    this->rooms.back().AddGuest(Guest(name,age,gender,this->rooms.size()));
    std::cout << "Finish";
  }
}

void Hotel::ShowRooms() {
  std::cout << "asdas";
  std::cout << this->rooms.size();
  for (int i = 0; i < this->rooms.size(); ++i) {
    std::cout << "Room capacity is " << this->rooms[i].Capacity();
    this->rooms[i].ShowGuests();
  }
}

Hotel::~Hotel() = default;