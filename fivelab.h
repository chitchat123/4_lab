//
// Created by vlad on 5/12/20.
//

#ifndef LAB15_4_LAB_FIVELAB_H_
#define LAB15_4_LAB_FIVELAB_H_

#include "Guest.h"
#include "Employee.h"

class fivelab : public Employee, public Guest{
 public:
  fivelab(std::string name,
          unsigned short age,
          char gender,
          std::string position,
          const fivelab &five);
  fivelab(
          unsigned short room_num = 10,
          std::string name = "Max",
          unsigned short age = 25,
          char gender = 'M',
          std::string position = "noone");
  static void seeText();
};

#endif //LAB15_4_LAB_FIVELAB_H_
