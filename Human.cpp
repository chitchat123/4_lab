//
// Created by chitchat on 3/27/20.
//

#include "Human.h"

std::string Human::Name() {
  return name;
}
unsigned short Human::Age() {
  return age;
}

char Human::Gender() {
  return gender;
}

void Human::Name(std::string name) {
  this->name = name;
}

void Human::Age(unsigned short age) {
  this->age = age;
}

void Human::Gender(char gender) {
  this->gender = gender;
}

Human::Human(std::string name, unsigned short age, char gender) : name(std::move(name)), age(age), gender(gender) {
}
