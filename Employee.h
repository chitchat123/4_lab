//
// Created by chitchat on 3/29/20.
//

#ifndef LAB4__EMPLOYEE_H_
#define LAB4__EMPLOYEE_H_

#include <string>
#include "Human.h"
class Employee : public Human {
 public:
  Employee(std::string name, unsigned short age, char gender, std::string position);
  void Position(std::string position);
  std::string Position();
  void DoWork();

  void seeText();
 private:
  std::string position;
};

#endif //LAB4__EMPLOYEE_H_
