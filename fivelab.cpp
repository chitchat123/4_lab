//
// Created by vlad on 5/12/20.
//

#include "fivelab.h"
#include <iostream>

fivelab::fivelab(
                 unsigned short room_num,
                 std::string name,
                 unsigned short age,
                 char gender,
                 std::string position) : Guest(name, age, gender, room_num),
                                         Employee(name, age, gender, position) {}

void fivelab::seeText() {
  std::cout << "\n fivelab";
}
